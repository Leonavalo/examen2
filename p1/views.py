from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q
from django.http import JsonResponse, HttpResponse
from django.core import serializers

from .models import po1
from .forms import po1Form

#Aqui las vistas
#Create
class Crear(generic.CreateView):
	template_name = "p1/crear.html"
	model = po1
	form_class = po1Form
	success_url = reverse_lazy("list")

#RETRIEVE

class Lista(generic.ListView):
    template_name = "p1/lista.html"
    model = po1
class Listauno(generic.ListView):
    template_name = "p1/list.html"
    queryset = po1.objects.filter(categoria="Ciencias Computacionales")
class Listados(generic.ListView):
    template_name = "p1/list.html"
    queryset = po1.objects.filter(categoria="Ciencias de la tierra")
class Listatres(generic.ListView):
    template_name = "p1/list.html"
    queryset = po1.objects.filter(categoria="Ciencias Naturales")
class Listacuatro(generic.ListView):
    template_name = "p1/list.html"
    queryset = po1.objects.filter(categoria="Ciencias Sociales")
class Listacinco(generic.ListView):
    template_name = "p1/list.html"
    queryset = po1.objects.filter(categoria="Ciencias Medicas")

class Detalle(generic.DetailView):
    template_name = "p1/detalle.html"
    model = po1

#UPDATE

class Update(generic.UpdateView):
	template_name = "p1/update.html"
	model = po1
	fields = [
        "nombreinv",
        "categoria",
        "autores",
        "slug",
    ]
	success_url = reverse_lazy("lista")

#Delete

class Delete(generic.DeleteView):
	template_name = "p1/delete.html"
	model = po1
	success_url = reverse_lazy("lista")
