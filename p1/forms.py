from django import forms

from .models import po1

class po1Form(forms.ModelForm):
    class Meta:
        model = po1
        fields = [
        "titulo",
        "categoria",
        "autores",
        "slug",
        ]
