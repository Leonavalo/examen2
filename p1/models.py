from django.db import models

# Create your models here.
from django.conf import settings

class po1(models.Model):
    titulo = models.CharField(max_length=100)
    autores = models.CharField(max_length=100)
    categoria = models.CharField(max_length=100)
    archivo = models.FileField(null=True, blank=True)
    slug = models.SlugField()

    def __str__(self):
        return self.nombreinv
