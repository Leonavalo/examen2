from django.urls import path

from p1 import views

urlpatterns = [
	path('lista/', views.Lista.as_view(), name="lista"),
    path('lista/1', views.Listauno.as_view(), name="listauno"),
    path('lista/2', views.Listados.as_view(), name="listados"),
    path('lista/3', views.Listatres.as_view(), name="listatres"),
    path('lista/4', views.Listacuatro.as_view(), name="listacuatro"),
    path('lista/5', views.Listacinco.as_view(), name="listacinco"),
	path('crear/', views.Crear.as_view(), name="crear"),
	path('detalle/<int:pk>/', views.Detalle.as_view(), name="detalle"),
	path('delete/<int:pk>/', views.Delete.as_view(), name="delete"),
	path('update/<int:pk>/', views.Update.as_view(), name="update"),
]
